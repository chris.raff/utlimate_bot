if __name__ == 'main':
	main()

def main():
    import cPickle
    data = cPickle.load(open('params.p', 'rb'))
    
    population = 100
    bots = [mybot(data)] * population
    
    carry = None
    for bot in bots:
        if carry == None:
            carry = bot
        else:
            match_play(bot, carry)
            # not quite sure about how this loop will work exactly
            
# returns 0 for bot1 or 1 for bot2
def play_match(bot1, bot2)
    wins = [0, 0, 0]
    while sum(wins) < 4 or wins[0] == wins[1]:
        outcome1 = play_game(bot1, bot2)
        outcome2 = play_game(bot2, bot1)
        # reverse the winner of outcome2
        if outcome2 < 2:
            outcome2 = 2 - outcome2
        for n in [outcome1, outcome2]:
            wins[n] += 1
    if wins[0] > wins[1]:
        return 0
    return 1
   
# returns 0 for bot1 win, 1 for bot2 win, or 3 for tie
def play_game(bot1, bot2)
    return 0 or 1 or 2