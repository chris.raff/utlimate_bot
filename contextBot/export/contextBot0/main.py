

def parse_command(instr, bot, pos):
    if instr.startswith('action move'):
        time = int(instr.split(' ')[-1])
        x, y = bot.get_move(pos, time)
        return 'place_move %d %d\n' % (x, y)
    elif instr.startswith('update game field'):
        fstr = instr.split(' ')[-1]
        pos.parse_field(fstr)
    elif instr.startswith('update game macroboard'):
        mbstr = instr.split(' ')[-1]
        pos.parse_macroboard(mbstr)
    elif instr.startswith('update game move'):
        pos.nmove = int(instr.split(' ')[-1])
    elif instr.startswith('settings your_botid'):
        myid = int(instr.split(' ')[-1])
        bot.myid = myid
        bot.oppid = 1 if myid == 2 else 2
    elif instr.startswith('settings timebank'): 
        bot.timebank = int(instr.split(' ')[-1])
    elif instr.startswith('settings time_per_move'): 
        bot.time_per_move = int(instr.split(' ')[-1])
    elif instr.startswith('snoopSaveQ'):
        bot.saveQ()
    elif instr.startswith('snoopMutate'):
        bot.mutate()
    elif instr.startswith('snoopWin'):
        #bot.saveQ()
        #bot.reward(100.0)
        reset()
    elif instr.startswith('snoopLose'):
        #bot.reward(-500.0)
        reset()
    elif instr.startswith('snoopTie'):
        #bot.reward(-50)
        reset()
    
    return ''
 
def reset():
    #pos = Position()
    #bot.moveHist = []
    #bot.boardHist = []
    #bot.histInd = 0
    pos.board = [0 for _ in range(81)]
    pos.macroboard = [-1 for _ in range(9)]

if __name__ == '__main__':
    import sys
    import cPickle
    from position import Position
    from mybot import MyBot
    
    data = [2.3013141127026246, 1.7905494026351951, 1.3637186900759397, 0.26084519183247407, 0.043254501999642474, 0.3780708030706485, 0.44536206745465606, 0.4318372160704244, 0.24346526921243497]
    
    pos = Position()
    bot = MyBot(data)
    
    while True:
        try:
            instr = raw_input()
        except Exception as e:
            sys.stderr.write('error reading input')
        outstr = parse_command(instr, bot, pos)
        sys.stdout.write(outstr)
        sys.stdout.flush()
            