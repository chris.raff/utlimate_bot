from random import random, randint
import cPickle
import sys

class MyBot:
    def __init__(self, data = [10.0, 8.0, 3.0, 1.0, 1.5, 5.0, 25.0, 20.0, 15.0]):
        self.data = data
        self.epsilon = 0.25
        
    
    def get_move(self, pos, tleft):
        
        macroState = board_state_macro(pos)
        microStates = [board_utility_micro(n, pos, macroState, self.data, self.myid) if pos.macroboard[n] == -1 else [-900.0]*9  for n in range(9)]
        
        a = []
        for n in range(81):
            tup = fieldNum_to_tuple(n)
            macS = macroSquare(tup)
            micS = microSquare(tup)
            a += [microStates[macS][micS]]
            
        #with open("thing", "w") as file:
        #    file.write(smart(a))
        #    file.close()
        
        #find action
        lmoves = pos.legal_moves()
        picks = []
        bestUtility = -1000
        for a in lmoves:
            macS = macroSquare(a)
            micS = microSquare(a)
            utility = microStates[macS][micS]
            if utility > bestUtility:
                bestUtility = utility
                picks = []
            if utility == bestUtility:
                picks += [a]
                
        return picks[0] if len(picks) == 1 else picks[randint(0, len(picks)-1)]
     
    def reward(self, reward):
        for n in range(self.histInd):
            boardAtN = self.boardHist[n]
            actionAtN= tuple_to_fieldNum(self.moveHist[n])
            histPow = self.histInd - n - 1
            self.q[boardAtN][actionAtN] += reward*pow(self.alpha, histPow)
       
    def saveQ(self):
        cPickle.dump( self.data, open( "params.p", "wb" ))
    def mutate(self):
        #for n in range(len(self.data)):
        #    self.data[n] += (random() - 0.5)*self.epsilon*self.data[n]
        n = randint(0, len(self.data)-1)
        self.data[n] += (random() - 0.5)*self.epsilon*self.data[n]
        
def tuple_to_fieldNum(tup):
    return tup[0] + 9*tup[1]

#def convert_board(pos):
#    retArr = [n for n in pos.board]
#    for n in pos.macroboard:
#        if pos.board[n] > 0: # macro square is owned by player
#            t = int((n%3)*3+round(n/3)*27)
#            toMod = [t, t+1, t+2,  t+9, t+10, t+11,  t+18, t+19, t+20]
#            for s in toMod:
#                retArr[s] = pos.board[n]
#    
#    return tuple(retArr)
   
#0:not immediate, 1:1 can take, 2:2 can take, 3:both can take, 5:1 took, 6:2 took
def board_state_macro(pos):
    out = [0 for _ in range(9)]
    
    for n in range(9):
        if pos.macroboard[n] > 0:
            out[n] = 3+pos.macroboard[n] #1 took, 2 took
        else:
            canTake = [False, False]
            
            for s in [[0, 1, 2], [9, 10, 11], [18, 19, 20], [0, 9, 18], [1, 10, 19], [2, 11, 20], [0, 10, 20], [2, 10, 18]]:
                hits = [0, 0]
                
                macTup = (n%3, n/3)
                macTup = (macTup[0]*3, macTup[1]*3)
                mac = tuple_to_fieldNum(macTup)
                
                for i in s:
                    owner = pos.board[mac+i]
                    if owner > 0:
                        hits[owner - 1] += 1
                
                for i in range(2):
                    if hits[i] >= 2:
                        canTake[i] = True
                #else:
                #    takable = blockable = setabe = False
                
                if hits[0] >= 2:
                    if hits[1] >= 2:
                        out[n] = 3 # both can take
                    else:
                        out[n] = 1 # 1 can take
                elif hits[1] >= 2:
                    out[n] = 2 # 2 can take
                
    return out
    
def board_utility_micro(macroS, pos, macroState, data, myid):
    out = [0.0]*9
    
    macTup = (macroS%3, macroS/3)
    macTup = (macTup[0]*3, macTup[1]*3)
    mac = tuple_to_fieldNum(macTup)
    
    for n in range(9):
        squareOffset = n%3+9*(n/3)
        
        bLoc = mac + squareOffset
            
        if pos.board[bLoc] == 0:
            spot = fieldNum_to_tuple(bLoc)
            takable = False
            blockable = False
            setable = False
            
            for s in [[0, 1, 2], [9, 10, 11], [18, 19, 20], [0, 9, 18], [1, 10, 19], [2, 11, 20], [0, 10, 20], [2, 10, 18]]:
            #for s in [[9, 10, 11], [18, 19, 20], [0, 9, 18], [1, 10, 19], [0, 10, 20]]:
                canTake = True
                canBlock = True
                canSet = False
                cantSet = False
                partOf = False
                
                for i in s:
                    isSelf = False
                    if mac + i == bLoc:
                        partOf = True
                        isSelf = True
                    owner = pos.board[mac+i]
                    if not isSelf:
                        if owner <= 0:
                            canTake = False
                            canBlock = False 
                        elif owner == myid:
                            canBlock = False
                            canSet = True
                        else:
                            #must be opponents
                            canTake = False
                            cantSet = True
                
                if partOf:
                    if canTake: takable = True
                    if canBlock: blockable = True
                    if canSet and not cantSet: setable = True
                #else:
                #    takable = blockable = setabe = False
                
            if takable:
                out[n] = data[0]
            elif blockable:
                out[n] = data[1]
            elif setable:
                out[n] = data[2]
            # logic for setting nextMove
            if macroState[n] > 0:
                num = macroState[n]
                if num in [3-myid, 3]: #if op can take it
                    out[n] -= data[7]#nextGiveF
                elif num == myid:
                    out[n] -= data[8]#nextBlockF
                # The square must already be claimed
                else:
                    out[n] -= data[6]#nextF
            # tiny bias for corners
            if n in [0, 2, 6, 8]:
                out[n] += data[4]
            #smaller bias for sides
            elif n in [1, 3, 5, 7]:
                out[n] += data[3]
            
            # logic for setting the next move to this microBoard
            if n == macroS:
                if macroState[n] in [myid, 3]: #if this square can be claimed this turn
                    out[n] -= data[6] #nextF
                else:
                    out[n] -= data[5]
    return out
                    
def fieldNum_to_tuple(num):
    return (num%9, num/9)

def macroSquare(tup):
    return tup[0]/3 + 3*(tup[1]/3)
def microSquare(tup):
    return tup[0]%3 + 3*(tup[1]%3)
 
def smart(a):
    a = [str(n) + ' ' for n in a]
    msg = ''
    for i in range(0, 81, 9):
        if not i % 27 and i > 0:
            msg += '---+---+---\n'

        msg += '|'.join([
            ''.join(a[i:i+3]),
            ''.join(a[i+3:i+6]),
            ''.join(a[i+6:i+9])]) + '\n'
    return msg
    
class Alignment:
    rot = [[]] * 4
    
    rot[0] = [n for n in range(9)]
    rot[1] = [(2+n*3 - (n/3))%9 for n in range(9)]
    rot[2] = [rot[1][rot[1][n]] for n in range(9)]
    rot[3] = [rot[2][rot[1][n]] for n in range(9)]
    
    mir = [(3*((n/3)+1)-n%3-1) for n in range(9)]
    
    def __init__(self, rot = 0, mirror = False):
        self.rotF = rot
        self.mirror = mirror
        
    def transform(self, board):
        retB = [board[self.rot[self.rotF][n]] for n in range(9)]
        if self.mirror:
            retB = [retB[self.mir[n]] for n in range(9)]
        return tuple(retB)
    def num_transform(self, num):
        if self.mirror:
            num = self.mir[num]

        return self.rot[self.rotF][num]