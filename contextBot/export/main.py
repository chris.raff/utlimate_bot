

def parse_command(instr, bot, pos):
    if instr.startswith('action move'):
        time = int(instr.split(' ')[-1])
        x, y = bot.get_move(pos, time)
        return 'place_move %d %d\n' % (x, y)
    elif instr.startswith('update game field'):
        fstr = instr.split(' ')[-1]
        pos.parse_field(fstr)
    elif instr.startswith('update game macroboard'):
        mbstr = instr.split(' ')[-1]
        pos.parse_macroboard(mbstr)
    elif instr.startswith('update game move'):
        pos.nmove = int(instr.split(' ')[-1])
    elif instr.startswith('settings your_botid'):
        myid = int(instr.split(' ')[-1])
        bot.myid = myid
        bot.oppid = 1 if myid == 2 else 2
    elif instr.startswith('settings timebank'): 
        bot.timebank = int(instr.split(' ')[-1])
    elif instr.startswith('settings time_per_move'): 
        bot.time_per_move = int(instr.split(' ')[-1])
    elif instr.startswith('snoopSaveQ'):
        bot.saveQ()
    elif instr.startswith('snoopMutate'):
        bot.mutate()
    elif instr.startswith('snoopWin'):
        #bot.saveQ()
        #bot.reward(100.0)
        reset()
    elif instr.startswith('snoopLose'):
        #bot.reward(-500.0)
        reset()
    elif instr.startswith('snoopTie'):
        #bot.reward(-50)
        reset()
    
    return ''
 
def reset():
    #pos = Position()
    #bot.moveHist = []
    #bot.boardHist = []
    #bot.histInd = 0
    pos.board = [0 for _ in range(81)]
    pos.macroboard = [-1 for _ in range(9)]

if __name__ == '__main__':
    import sys
    import cPickle
    from position import Position
    from mybot import MyBot
    
    data = [2.7259111056088554, 0.7901853186785578, 1.053494121109361, 0.10756023130668069, 0.004874984131382287, 0.19509728144612495, 0.5926113836626189, 0.19233589257146597, 0.09866390828432947]
    
    pos = Position()
    bot = MyBot(data)
    
    while True:
        try:
            instr = raw_input()
        except Exception as e:
            sys.stderr.write('error reading input')
        outstr = parse_command(instr, bot, pos)
        sys.stdout.write(outstr)
        sys.stdout.flush()
            