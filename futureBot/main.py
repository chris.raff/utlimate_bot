

def parse_command(instr, bot, pos):
    if instr.startswith('action move'):
        time = int(instr.split(' ')[-1])
        x, y = bot[0].get_move(pos, time)
        return 'place_move %d %d\n' % (x, y)
    elif instr.startswith('update game field'):
        fstr = instr.split(' ')[-1]
        move = pos.parse_field(fstr, bot[0].myid)
        if not move == None:
            bot[0].opponent_move(move)
    elif instr.startswith('update game macroboard'):
        mbstr = instr.split(' ')[-1]
        pos.parse_macroboard(mbstr)
    elif instr.startswith('update game move'):
        pos.nmove = int(instr.split(' ')[-1])
    elif instr.startswith('settings your_botid'):
        myid = int(instr.split(' ')[-1])
        bot[0].myid = myid
        bot[0].oppid = 1 if myid == 2 else 2
    elif instr.startswith('settings timebank'): 
        bot[0].timebank = float(instr.split(' ')[-1])/1000
    elif instr.startswith('settings time_per_move'): 
        bot[0].time_per_move = float(int(instr.split(' ')[-1]))/1000
    elif instr.startswith('snoopSaveQ'):
        pass
        #bot[0].saveQ()
    elif instr.startswith('snoopMutate'):
        pass
        #bot[0].mutate()
    elif instr.startswith('snoopWin'):
        reset(bot)
    elif instr.startswith('snoopLose'):
        reset(bot)
    elif instr.startswith('snoopTie'):
        reset(bot)
    
    return ''
    
# call reset([bot]) so that the 
def reset(bot):
    #bot[0].clean_up()
    id = bot[0].myid
    timebank = bot[0].timebank
    time_per_move = bot[0].time_per_move
    bot[0] = MyBot()
    bot[0].myid = id
    bot[0].timebank = timebank
    bot[0].time_per_move = time_per_move
    pos.board = [0 for _ in range(81)]
    pos.macroboard = [-1 for _ in range(9)]

if __name__ == '__main__':
    import sys
    import cPickle
    from position import Position
    from mybot import MyBot
    
    pos = Position()
    bot = [MyBot()]
    
    pos.board = [0 for _ in range(81)]
    pos.macroboard = [-1 for _ in range(9)]
    
    while True:
        try:
            instr = raw_input()
        except Exception as e:
            sys.stderr.write('error reading input')
        outstr = parse_command(instr, bot, pos)
        sys.stdout.write(outstr)
        sys.stdout.flush()
            