from random import random, randint
import sys
import time
import threading
import Queue
from lpos import BoardNode, LPos

class MyBot:
    def __init__(self):
        self.queue = Queue.Queue()
        self.root = None
        self.moves = [] # record moves,
        self.moven = 0
        
        self.t = threading.Thread(target = self.search)
        self.t.setDaemon(True)
        self.lock = threading.Lock()
    
    def get_move(self, pos, tleft):
        start_time = time.time()
        
        tleft = float(tleft)/1000
        
        if self.root == None:
            self.root = BoardNode(LPos(pos.board, pos.macroboard), self.myid)
            self.queue.put(self.root)
            self.t.start()
            
        # get the utility from the tree - this can be time consuming
        s_moves = []
        with self.lock:
            s_moves = self.root.utility(timeby = start_time + tleft * 0.95) #consider subtracting instead of multiplying
            if len(s_moves) == 1: # expand the root and call utility again
                tQ = self.root.expand()
                for b in tQ:
                    self.queue.put(b)
                s_moves = self.root.utility()
                
        
        # change tleft to be the remaining compute time
        tleft -= time.time() - start_time
        
        # sleep to let the search make the bot smart
        if self.t.is_alive():
            if self.moven == 0:
                time.sleep(max(0, self.time_per_move + tleft - self.timebank))
            else: time.sleep(pow(float(max(0, tleft))/self.timebank, 3)*6*pow(float(self.moven)/81, 1.0/3)) #consider making the power and maximum bigger for a more aggressive curve
        
        num = pick_move(s_moves)
        move = (num%9, num/9)
        
        self.moves += [move]
        self.moven += 1
        
        #make move locally
        self.root = self.root.children[move]
        
        return move
        
    def opponent_move(self, move):
        with self.lock:
            if self.moven > 0:
                self.moven+=1
                self.moves += [move]
            if not self.root == None:
                if len(self.root.children) == 0:
                    tQ = self.root.expand()
                    for b in tQ:
                        self.queue.put(b)
                try:
                    self.root = self.root.children[move]
                except Exception:
                    raise Exception('children: {}\ntried {}'.format(self.root.children, move))

    def search(self):
        while not self.queue.empty():
            with self.lock:
                board = self.queue.get()
                if board.instructions[:self.moven] == self.moves[:self.moven]:
                    tQ = board.expand()
                    for m in tQ:
                        self.queue.put(m)
       
    def mutate(self):
        pass

def pick_move(utils):
    top = []
    best_utility = None
    for n in range(len(utils)):
        if utils[n] > best_utility:
            best_utility = utils[n]
            top = []
        if utils[n] == best_utility:
            top += [n]
    from random import randrange
    return top[0] if len(top) == 1 else top[randrange(len(top))]