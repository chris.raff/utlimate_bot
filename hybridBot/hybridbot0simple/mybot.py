from random import random, randint
import sys
import time
import threading
import Queue
from lpos import BoardNode, LPos
import contextbot

class MyBot:
    def __init__(self):
        self.queue = Queue.Queue()
        self.root = None
        self.moves = [] # record moves,
        self.moven = 0
        
        self.t = threading.Thread(target = self.search)
        self.t.setDaemon(True)
        self.lock = threading.Lock()
    
    def get_move(self, pos, tleft):
        tleft = float(tleft)/1000
        
        if self.root == None:
            self.root = BoardNode(LPos(pos.board, pos.macroboard), self.myid)
            self.queue.put(self.root)
            self.t.start()
        
        # sleep to let the search make the bot smart
        if tleft == self.timebank:
            if self.moven == 0:
                time.sleep(self.time_per_move*0.8)
            else: time.sleep(min(float(self.moven)/81 * self.timebank*1.1, self.timebank*9.0/10))
        else: time.sleep(self.time_per_move*0.5)
        
        s_moves = []
        with self.lock:
            s_moves = self.root.utility()
            if len(s_moves): # expand the root and call utility again
                tQ = self.root.expand()
                for b in tQ:
                    self.queue.put(b)
                s_moves = self.root.utility()
            
        if 1000 in s_moves or self.moven in [0, 1]:
            num = pick_move(s_moves)
            move = (num%9, num/9)
            
            self.moves += [move]
            self.moven += 1
            
            self.root = self.root.children[move]
            
            return move
            
        utilityBoard = contextbot.getUtils(pos, self.myid)
        lmoves = pos.legal_moves()
        picks = []
        bestUtility = -1000
        for a in lmoves:
            square = a[0]+9*a[1]
            utility = utilityBoard[square]
            if utility > bestUtility:
                bestUtility = utility
                picks = []
            if utility == bestUtility:
                picks += [a]
            
        move =  picks[0] if len(picks) == 1 else picks[randint(0, len(picks)-1)]
        
        if s_moves[move[0] + 9*move[1]] == -1000:
            move = pick_move(s_moves)
            
            self.moves += [move]
            self.moven += 1
            
            self.root = self.root.children[move]
            
            return move
            
        
        if len(self.root.children) == 0:
            with self.lock:
                tQ = self.root.expand()
                for b in tQ:
                    self.queue.put(b)
                    
        #may cause threading issues
        self.moves += [move]
        self.moven += 1

        #make move locally
        self.root = self.root.children[move]
        
        return move
        
    def opponent_move(self, move):
        with self.lock:
            if self.moven > 0:
                self.moven+=1
                self.moves += [move]
            if not self.root == None:
                if len(self.root.children) == 0:
                    tQ = self.root.expand()
                    for b in tQ:
                        self.queue.put(b)
                try:
                    self.root = self.root.children[move]
                except Exception:
                    raise Exception('children: {}\ntried {}'.format(self.root.children, move))

    def search(self):
        while not self.queue.empty():
            with self.lock:
                board = self.queue.get()
                if board.instructions[:self.moven] == self.moves[:self.moven]:
                    tQ = board.expand()
                    for m in tQ:
                        self.queue.put(m)
       
    def mutate(self):
        pass

def pick_move(utils):
    top = []
    best_utility = None
    for n in range(len(utils)):
        if utils[n] > best_utility:
            best_utility = utils[n]
            top = []
        if utils[n] == best_utility:
            top += [n]
    from random import randrange
    return top[0] if len(top) == 1 else top[randrange(len(top))]