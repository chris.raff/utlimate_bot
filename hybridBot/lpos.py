class LPos:
    
    def __init__(self, board = [0]*81, macroboard = [-1]*9):
        # duplicate arrays for safety
        self.board = [n for n in board]
        self.macroboard = [n for n in macroboard]
        self.done = False
    
    def is_legal(self, x, y):
        mbx, mby = int(x)/3, int(y)/3
        return self.macroboard[3*mby+mbx] == -1 and self.board[9*y+x] == 0

    def legal_moves(self):
        out = [] if self.done else[ (x, y) for x in range(9) for y in range(9) if self.is_legal(x, y) ]
        if out == [(x, y) for x in range(9) for y in range(9)]: out = [(4, 4)]
        return out
        
    def make_move(self, x, y, pid):
        self.board[9*y+x] = pid
        
        self.macroboard_set()
        
        util = self.eval_utility(1)
        if util in [1000, -1000]:
            self.done = True
        
        mbx, mby = x%3, y%3
        mb = 3*mby+mbx
        if self.macroboard[mb] == 0:
            self.macroboard[mb] = -1
        else:
            for n in range(9):
                if self.macroboard[n] == 0:
                    self.macroboard[n] = -1
        
    
    def apply_move(self, x, y, pid):
        b = LPos(self.board, self.macroboard)
        b.make_move(x, y, pid)
        return b
        
    def macroboard_set(self):
        for n in range(9):
            self.macroboard[n] = 3
            for s in [[0, 1, 2], [9, 10, 11], [18, 19, 20], [0, 9, 18], [1, 10, 19], [2, 11, 20], [0, 10, 20], [2, 10, 18]]:
            
                hits = [0, 0]
                
                mac = 3*(n%3) + 9*3*(n/3)
                
                for i in s:
                    if self.board[mac+i] > 0:
                        hits[self.board[mac+i] - 1] += 1
                    elif self.macroboard[n] == 3:
                        self.macroboard[n] = 0
                
                for i in range(2):
                    if hits[i] == 3:
                        self.macroboard[n] = i+1
                
            
        
    def eval_utility(self, pid):
        out = 0
        #subOut = [[0]*9]*2 #importance of unclaimed microboards
        
        myOwner = pid - 1
        opOwner = 2 - pid
        
        for s in [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]:
            hits = [0, 0, 0]
            
            for i in s:
                owner = self.macroboard[i]
                if owner > 0:
                    hits[owner-1] += 1
            
            #pid win
            if hits[myOwner] == 3:
                return 1000
            #op win
            if hits[opOwner] == 3:
                return -1000
                
            #pid setup
            if hits[myOwner] == 2 and hits[opOwner] == 0 and hits[2] == 0:
                out += 10
            #op setup
            if hits[opOwner] == 2 and hits[myOwner] == 0 and hits[2] == 0:
                out -= 10.1
                
            #pid square with potential
            if hits[myOwner] == 1 and hits[opOwner] == 0 and hits[2] == 0:
                out += 1
            #op square with potential
            if hits[opOwner] == 1 and hits[myOwner] == 0 and hits[2] == 0:
                out -= 1.01
            
            
            '''#pid square that may be important
            if hits[opOwner] == 0 and hits[2] == 0:
                for i in s:
                    if self.macroboard[i] == 0: subOut[0][i] += 1
            
            #op square that may be important
            if hits[myOwner] == 0 and hits[2] == 0:
                for i in s:
                    if self.macroboard[i] == 0: subOut[1][i] += 1'''
        '''for n in range(9):
            if max(subOut[0][n], subOut[1][n]) > 0: # board has significance
                mac = 3*(n%3) + 9*3*(n/3)
                for s in [[0, 1, 2], [9, 10, 11], [18, 19, 20], [0, 9, 18], [1, 10, 19], [2, 11, 20], [0, 10, 20], [2, 10, 18]]:
                    hits = [0, 0]
                    for i in s:
                        owner = self.board[mac+i]
                        if owner > 0:
                            hits[owner - 1] += 1
                    
                    if hits[opOwner] == 0:
                        out += subOut[0][n]*0.0022*hits[myOwner]
                    if hits[myOwner] == 0:
                        out -= subOut[1][n]*0.0023*hits[opOwner]'''
        
        return out

        
class BoardNode:
    def __init__(self, board = LPos(), mid = 1, instructions = []):
        self.board = board
        self.moveid = mid # The player who's move it is on this board. IE If empty, first player
        self.children = {} # map of move tuples to boardnodes
        self.util = None
        self.instructions = instructions
        
    # returns a (utility, move) tuple. The move component is only relavent when evaluated from the current board
    def utility(self, pid = None, timeby = None):
        if pid == None:
            pid = self.moveid
        
        from time import time
        if len(self.children) == 0 or timeby > time():
            out = [self.board.eval_utility(pid)]
        else:
            out = [None] * 81
            for m in self.children:
                mUt = self.children[m].utility(pid, timeby) # only call this once per board, it's very expensive.
                bestUt = None
                condition = False
                
                for u in mUt:
                    if pid == self.moveid:
                        condition = bestUt > mUt
                    else: condition = bestUt < mUt #minimize utility for other player
                    if condition or bestUt == None:
                        bestUt = mUt
            
                out[m[0] + m[1]*9] = bestUt
        return out
    
    # returns a list of board nodes to enqueue
    def expand(self):
        
        lmoves = self.board.legal_moves()
        out = []
        for m in lmoves:
            board = self.board.apply_move(m[0], m[1], self.moveid)
            self.children[m] = BoardNode(board, 3 - self.moveid, [i for i in self.instructions] + [m])
            out+= [self.children[m]]
        return out