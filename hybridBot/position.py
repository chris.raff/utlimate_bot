
class Position:
    
    def __init__(self):
        self.board = [0 for _ in range(81)]
        self.macroboard = [-1 for _ in range(9)]
    
    def parse_field(self, fstr, myid):
        flist = fstr.replace(';', ',').split(',')
        board = [ int(f) for f in flist ]
        move = -1
        for n in range(81):
            if len(self.board) == 81 and (not board[n] == self.board[n]) and board[n] == 3-myid:
                move = n
                break
        self.board = board
        return (move%9, move/9) if move > -1 else None
    
    def parse_macroboard(self, mbstr):
        mblist = mbstr.replace(';', ',').split(',')
        self.macroboard = [ int(f) for f in mblist ]
    
    def is_legal(self, x, y):
        mbx, mby = int(x)/3, int(y)/3
        try:
            return self.macroboard[3*mby+mbx] == -1 and self.board[9*y+x] == 0
        except Exception as e:
            raise Exception('Error: {}, {}. mbx, mby = {}, {}'.format(x, y, mbx, mby))

    def legal_moves(self):
        return [ (x, y) for x in range(9) for y in range(9) if self.is_legal(x, y) ]
        
    def make_move(self, x, y, pid):
        mbx, mby = x/3, y/3
        self.macroboard[3*mby+mbx] = -1
        self.board[9*y+x] = pid
        
    def get_board(self):
        return ''.join(self.board, ',')

    def get_macroboard(self):
        return ''.join(self.macroboard, ',')
    