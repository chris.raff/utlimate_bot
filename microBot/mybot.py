from random import random, randint
import cPickle
import sys

class MyBot:
    def __init__(self, q = {}):
        self.q = q
        
        self.boardHist = [[]]*10 # the tenth is macroboard
        self.boardRewarded = [False]*9
        self.moveHist = []
        self.histInd = 0
        
        self.alpha = 0.01
        
        #alpha or "the blame in the past factor" should be really end-heavy after it's learned for while, and really all-game-blaming at the beginning
        #consider the ai playing a perfect game and then choking at the end
        #It shouldn't punish the whole game
    
    def get_move(self, pos, tleft):
        #note moves are tuples
        boardTuple = convert_board(pos)
        q = self.q
        
        enumMap = [Alignment(), Alignment(1), Alignment(2), Alignment(3), Alignment(0, True), Alignment(1, True), Alignment(2, True), Alignment(3, True)]
        al = enumMap[0]
        
        notPresent = True
        for a in enumMap:
            if a.transform(boardTuple) in q:
                al = a
                notPresent = False
                break
                
        #if not notPresent:
            #with open("Output.txt", "a") as text_file:
            #    text_file.write('Board seen, alignment is rot {}, mirror {}\n'.format(al.rotF, al.mirror))
                
        boardTuple = al.transform(boardTuple)
        
        if notPresent and not boardTuple in q:
            #q[boardTuple] = [0.0 for _ in range(82)]
            #q[boardTuple][81] = 1 # epsilon per board state. Starts high to encourage exploration on new boardstates
            q[boardTuple] = board_rules(pos, self.myid)
            #with open("Output.txt", "w") as text_file:
            #    text_file.write('{}'.format(smart(q[boardTuple])))
            
        currentArray = q[boardTuple]
        
        # reduce epsilon
        currentArray[81] *= 0.995
        
        #add boardstate to histories
        self.boardHist += [boardTuple]
        
        #find action
        lmoves = pos.legal_moves()
        action = ()
        if currentArray[81] > random():
            action = lmoves[randint(0, len(lmoves)-1)]
        else:
            action = lmoves[0]
            for a in lmoves:
                if currentArray[al.num_transform(tuple_to_fieldNum(action))] < currentArray[al.num_transform(tuple_to_fieldNum(a))]:
                    action = a
                    
                    
                
            
        #add action to moveHist
        self.moveHist += [action]
        
        #increase histInd
        self.histInd += 1
        
        return action
     
    def reward(self, reward):
        for n in range(self.histInd):
            boardAtN = self.boardHist[n]
            actionAtN= tuple_to_fieldNum(self.moveHist[n])
            histPow = self.histInd - n - 1
            self.q[boardAtN][actionAtN] += reward*pow(self.alpha, histPow)
       
    def saveQ(self):
        cPickle.dump( self.q, open( "qDict.p", "wb" ))
        
def tuple_to_fieldNum(tup):
    return tup[0] + 9*tup[1]

def convert_board(pos):
    retArr = [n for n in pos.board]
    for n in pos.macroboard:
        if pos.board[n] > 0: # macro square is owned by player
            t = int((n%3)*3+round(n/3)*27)
            toMod = [t, t+1, t+2,  t+9, t+10, t+11,  t+18, t+19, t+20]
            for s in toMod:
                retArr[s] = pos.board[n]
    
    return tuple(retArr)

#from highest to lowest: myid can take, myid can block, anything else
def utility(tBoard, myid):
    return 0
def  microUtility(mac, mic, myid):
    return 0
   
#not immediate, 1 can take, 2 can take, both can take, 1 took, 2 took
def board_state_macro(macroS, pos):
    out = [0 for _ in range(9)]
    
    for n in range(9):
        if pos.macroBoard[n] > 0:
            out[n] = 3+pos.macroBoard[n] #1 took, 2 took
        else:
            canTake = [False, False]
            
            for s in [[0, 1, 2], [9, 10, 11], [18, 19, 20], [0, 9, 18], [1, 10, 19], [2, 11, 20], [0, 10, 20], [2, 10, 18]]:
                hits = [0, 0]
                
                macTup = (n%3, n/3)
                macTup = (macTup[0]*3, macTup[1]*3)
                mac = tuple_to_fieldNum(macTup)
                
                for i in s:
                    owner = pos.board[mac+i]
                    if owner > 0:
                        hits[owner - 1] += 1
                
                for i in range(2):
                    if hits[i] >= 2:
                        canTake[i] = True
                #else:
                #    takable = blockable = setabe = False
                
                if hits[0] >= 2:
                    if hits[1] >= 2:
                        out[n] = 3 # both can take
                    else:
                        out[n] = 1 # 1 can take
                elif hits[1] >= 2:
                    out[n] = 2 # 2 can take
                
    return out
    
def board_state_micro(macroS, pos):
    out = [3]*9 # both can take
    for n in range(9):
        macTup = (macroS%3, macroS/3)
        macTup = (macTup[0]*3, macTup[1]*3)
        mac = tuple_to_fieldNum(macTup)
        
        if board.pos[mac + n] > 0:
            out[n] = board.pos[mac + n] + 3
    return out
        

def tuple_bound_to_square_addition(tup, tupAdd):
    mac = macroSquare(tup)
    arr = [n for n in tup]
    arr[0] += tupAdd[0]
    arr[1]+= tupAdd[1]
    mic = microSquare(arr)
    return fieldNum_to_tuple(mac+mic)
    
def fieldNum_to_tuple(num):
    return (num%9, num/9)

def macroSquare(tup):
    return tup[0]/3 + 3*(tup[1]/3)
def microSquare(tup):
    return tup[0]%3 + 3*(tup[1]%3)
 
'''def smart(a):
    a = [str(n) + ' ' for n in a]
    msg = ''
    for i in range(0, 81, 9):
        if not i % 27 and i > 0:
            msg += '---+---+---\n'

        msg += '|'.join([
            ''.join(a[i:i+3]),
            ''.join(a[i+3:i+6]),
            ''.join(a[i+6:i+9])]) + '\n'
    return msg'''
    
class Alignment:
    rot = [[]] * 4
    
    rot[0] = [n for n in range(9)]
    rot[1] = [(2+n*3 - (n/3))%9 for n in range(9)]
    rot[2] = [rot[1][rot[1][n]] for n in range(9)]
    rot[3] = [rot[2][rot[1][n]] for n in range(9)]
    
    mir = [(3*((n/3)+1)-n%3-1) for n in range(9)]
    
    def __init__(self, rot = 0, mirror = False):
        self.rotF = rot
        self.mirror = mirror
        
    def transform(self, board):
        retB = [board[self.rot[self.rotF][n]] for n in range(9)]
        if self.mirror:
            retB = [retB[self.mir[n]] for n in range(9)]
        return tuple(retB)
    def num_transform(self, num):
        if self.mirror:
            num = self.mir[num]

        return self.rot[self.rotF][num]
    
    
    