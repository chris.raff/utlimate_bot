import mybot as m
import position as p

cat = m.MyBot()
pos = p.Position()
pos.board = [0 for _ in range(81)]
pos.macroboard = [-1 for _ in range(9)]

print(cat.get_move(pos, 1000))
cat.reward(100)
print('done')