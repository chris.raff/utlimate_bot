from random import random, randint
import cPickle

class MyBot:
    def __init__(self, q = {}):
        self.q = q
        
        self.boardHist = []
        self.moveHist = []
        self.histInd = 0
        
        self.alpha = 0.01
        #alpha or "the blame in the past factor" should be really end-heavy after it's learned for while, and really all-game-blaming at the beginning
        #consider the ai playing a perfect game and then choking at the end
        #It shouldn't punish the whole game
    
    def get_move(self, pos, tleft):
        #note moves are tuples
        boardTuple = convert_board(pos)
        q = self.q
        
        enumMap = [Alignment(), Alignment(1), Alignment(2), Alignment(3), Alignment(0, True), Alignment(1, True), Alignment(2, True), Alignment(3, True)]
        al = enumMap[0]
        
        notPresent = True
        for a in enumMap:
            if a.transform(boardTuple) in q:
                al = a
                notPresent = False
                break
                
        boardTuple = al.transform(boardTuple)
        
        if notPresent and not boardTuple in q:
            q[boardTuple] = [0.0 for _ in range(82)]
            q[boardTuple][81] = 1 # epsilon per board state. Starts high to encourage exploration on new boardstates
            
        currentArray = q[boardTuple]
        
        # reduce epsilon
        currentArray[81] *= 0.995
        
        #add boardstate to histories
        self.boardHist += [boardTuple]
        
        #find action
        lmoves = pos.legal_moves()
        action = ()
        if currentArray[81]**150 > random():
            action = lmoves[randint(0, len(lmoves)-1)]
        else:
            action = lmoves[0]
            for a in lmoves:
                if currentArray[al.num_transform(tuple_to_fieldNum(action))] < currentArray[al.num_transform(tuple_to_fieldNum(a))]:
                    action = a
                    
                    
                
            
        #add action to moveHist
        self.moveHist += [action]
        
        #increase histInd
        self.histInd += 1
        
        return action
     
    def reward(self, reward):
        for n in range(self.histInd):
            boardAtN = self.boardHist[n]
            actionAtN= tuple_to_fieldNum(self.moveHist[n])
            histPow = self.histInd - n - 1
            self.q[boardAtN][actionAtN] += reward*pow(self.alpha, histPow)
       
    def saveQ(self):
        cPickle.dump( self.q, open( "qDict.p", "wb" ))
        
def tuple_to_fieldNum(tup):
    return tup[0] + 9*tup[1]

def convert_board(pos):
    retArr = [n for n in pos.board]
    for n in pos.macroboard:
        if pos.board[n] > 0: # macro square is owned by player
            t = int((n%3)*3+round(n/3)*27)
            toMod = [t, t+1, t+2,  t+9, t+10, t+11,  t+18, t+19, t+20]
            for s in toMod:
                retArr[s] = pos.board[n]
    
    return tuple(retArr)
    
'''def find_alignment(board):
    rot = 0
    mirror = False
    candidates = [True] * 8
    
    for n in range(5):
        lines = [[]] * 8
        for i in range(len(candidates)):
            if candidates[i]:
                thisAl =alignment.enumMap[
                line[i]
            else: lines[i] = []'''
        
class Alignment:
    rot = [[]] * 4
    
    rot[0] = [n for n in range(81)]
    rot[1] = [(8+n*9 - (n/9))%81 for n in range(81)]
    rot[2] = [rot[1][rot[1][n]] for n in range(81)]
    rot[3] = [rot[2][rot[1][n]] for n in range(81)]
    
    mir = [(9*((n/9)+1)-n%9-1) for n in range(81)]
    
    def __init__(self, rot = 0, mirror = False):
        self.rotF = rot
        self.mirror = mirror
        
    def transform(self, board):
        retB = [board[self.rot[self.rotF][n]] for n in range(81)]
        if self.mirror:
            retB = [retB[self.mir[n]] for n in range(81)]
        return tuple(retB)
    def num_transform(self, num):
        if self.mirror:
            num = self.mir[num]

        return self.rot[self.rotF][num]
        ''' if self.rotF%2 == 0 else (self.rotF+2)%4'''
    
    
    