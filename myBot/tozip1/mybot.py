from random import random, randint
import cPickle
import sys

class MyBot:
    def __init__(self, q = {}):
        self.q = q
        
        self.boardHist = []
        self.moveHist = []
        self.histInd = 0
        
        self.alpha = 0.01
        
        #alpha or "the blame in the past factor" should be really end-heavy after it's learned for while, and really all-game-blaming at the beginning
        #consider the ai playing a perfect game and then choking at the end
        #It shouldn't punish the whole game
    
    def get_move(self, pos, tleft):
        #note moves are tuples
        boardTuple = convert_board(pos)
        q = self.q
        
        enumMap = [Alignment(), Alignment(1), Alignment(2), Alignment(3), Alignment(0, True), Alignment(1, True), Alignment(2, True), Alignment(3, True)]
        al = enumMap[0]
        
        notPresent = True
        for a in enumMap:
            if a.transform(boardTuple) in q:
                al = a
                notPresent = False
                break
                
        #if not notPresent:
            #with open("Output.txt", "a") as text_file:
            #    text_file.write('Board seen, alignment is rot {}, mirror {}\n'.format(al.rotF, al.mirror))
                
        boardTuple = al.transform(boardTuple)
        
        if notPresent and not boardTuple in q:
            #q[boardTuple] = [0.0 for _ in range(82)]
            #q[boardTuple][81] = 1 # epsilon per board state. Starts high to encourage exploration on new boardstates
            q[boardTuple] = board_rules(pos, self.myid)
            #with open("Output.txt", "w") as text_file:
            #    text_file.write('{}'.format(smart(q[boardTuple])))
            
        currentArray = q[boardTuple]
        
        # reduce epsilon
        currentArray[81] *= 0.995
        
        #add boardstate to histories
        self.boardHist += [boardTuple]
        
        #find action
        lmoves = pos.legal_moves()
        action = ()
        if currentArray[81]**100 > random():
            action = lmoves[randint(0, len(lmoves)-1)]
        else:
            action = lmoves[0]
            for a in lmoves:
                if currentArray[al.num_transform(tuple_to_fieldNum(action))] < currentArray[al.num_transform(tuple_to_fieldNum(a))]:
                    action = a
                    
                    
                
            
        #add action to moveHist
        self.moveHist += [action]
        
        #increase histInd
        self.histInd += 1
        
        return action
     
    def reward(self, reward):
        for n in range(self.histInd):
            boardAtN = self.boardHist[n]
            actionAtN= tuple_to_fieldNum(self.moveHist[n])
            histPow = self.histInd - n - 1
            self.q[boardAtN][actionAtN] += reward*pow(self.alpha, histPow)
       
    def saveQ(self):
        cPickle.dump( self.q, open( "qDict.p", "wb" ))
        
def tuple_to_fieldNum(tup):
    return tup[0] + 9*tup[1]

def convert_board(pos):
    retArr = [n for n in pos.board]
    for n in pos.macroboard:
        if pos.board[n] > 0: # macro square is owned by player
            t = int((n%3)*3+round(n/3)*27)
            toMod = [t, t+1, t+2,  t+9, t+10, t+11,  t+18, t+19, t+20]
            for s in toMod:
                retArr[s] = pos.board[n]
    
    return tuple(retArr)
    
def board_rules(pos, myid):
    out = [0.0 for _ in range(82)]
    out[81] = 1.0
    reduce = 0.95
    for n in range(81):
        if pos.board[n] == 0:
            spot = fieldNum_to_tuple(n)
            aTups = [(1, 0), (0, 1)]
            takable = False
            blockable = False
            setable = False
            
            for s in [[0, 1, 2], [9, 10, 11], [18, 19, 20], [0, 9, 18], [1, 10, 19], [2, 11, 20], [0, 10, 20], [2, 10, 18]]:
                canTake = True
                canBlock = True
                canSet = False
                cantSet = False
                partOf = False
                
                mac = macroSquare(spot)
                macTup = (mac%3, mac/3)
                macTup = (macTup[0]*3, macTup[1]*3)
                mac = tuple_to_fieldNum(macTup)
                
                for i in s:
                    isSelf = False
                    if mac + i == n:
                        partOf = True
                        isSelf = True
                    owner = pos.board[mac+i]
                    if not isSelf:
                        if owner <= 0:
                            canTake = False
                            canBlock = False 
                        elif owner == myid:
                            canBlock = False
                            canSet = True
                        else:
                            #must be opponents
                            canTake = False
                            cantSet = True
                
                if partOf:
                    if canTake: takable = True
                    if canBlock: blockable = True
                    if canSet and not cantSet: setable = True
                #else:
                #    takable = blockable = setabe = False
                
            if takable:
                out[n] = 10.0
                out[81] *= reduce
            elif blockable:
                out[n] = 5.0
                out[81] *= reduce
            elif setable:
                out[n] = 2.5
            if pos.macroboard[microSquare(spot)] > 0:
                out[n] -= 10.0
                out[81] *= reduce
            # tiny bias for corners
            if microSquare(spot) in [0, 2, 6, 8]:
                out[n] += 2.0
            #smaller bias for sides
            elif microSquare(spot) in [1, 3, 5, 7]:
                out[n] += 1.0
    return out
            
        

def tuple_bound_to_square_addition(tup, tupAdd):
    mac = macroSquare(tup)
    arr = [n for n in tup]
    arr[0] += tupAdd[0]
    arr[1]+= tupAdd[1]
    mic = microSquare(arr)
    return fieldNum_to_tuple(mac+mic)
    
def fieldNum_to_tuple(num):
    return (num%9, num/9)

def macroSquare(tup):
    return tup[0]/3 + 3*(tup[1]/3)
def microSquare(tup):
    return tup[0]%3 + 3*(tup[1]%3)
 
'''def smart(a):
    a = [str(n) + ' ' for n in a]
    msg = ''
    for i in range(0, 81, 9):
        if not i % 27 and i > 0:
            msg += '---+---+---\n'

        msg += '|'.join([
            ''.join(a[i:i+3]),
            ''.join(a[i+3:i+6]),
            ''.join(a[i+6:i+9])]) + '\n'
    return msg'''
    
class Alignment:
    rot = [[]] * 4
    
    rot[0] = [n for n in range(81)]
    rot[1] = [(8+n*9 - (n/9))%81 for n in range(81)]
    rot[2] = [rot[1][rot[1][n]] for n in range(81)]
    rot[3] = [rot[2][rot[1][n]] for n in range(81)]
    
    mir = [(9*((n/9)+1)-n%9-1) for n in range(81)]
    
    def __init__(self, rot = 0, mirror = False):
        self.rotF = rot
        self.mirror = mirror
        
    def transform(self, board):
        retB = [board[self.rot[self.rotF][n]] for n in range(81)]
        if self.mirror:
            retB = [retB[self.mir[n]] for n in range(81)]
        return tuple(retB)
    def num_transform(self, num):
        if self.mirror:
            num = self.mir[num]

        return self.rot[self.rotF][num]
    
    
    