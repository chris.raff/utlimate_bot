from random import random, randint

class MyBot:
    def __init__(self, q = {}):
        self.q = q
        self.boardHist = []
        self.moveHist = []
        self.histInd = 0
        #alpha or "the blame in the past factor" should be really end-heavy after it's learned for while, and really all-game-blaming at the beginning
        #consider the ai playing a perfect game and then choking at the end
        #it shouldn't punish the whole game
    
    def get_move(self, pos, tleft):
        #note moves are tuples
        boardTuple = convert_board(pos)
        
        if not boardTuple in q:
            q[boardTuple] = [0 for _ in range(82)]
            q[boardTuple][81] = 1 # epsilon per board state. Starts high to encourage exploration on new boardstates
        
        #add boardstate to histories
        #find action
        lmoves = pos.legal_moves()
        action = ()
        if q[boardTuple][81] > random.random():
            action = lmoves[randint(0, len(lmoves)-1)]
        else:
            curr_state_rewards = q[boardTuple]
            action = lmoves[0]
            for a in lmoves:
                if curr_state_rewards[tuple_to_feildNum(action)] < curr_state_rewards[tuple_to_feildNum(a)]:
                    action = a
            
        #add action to moveHist
        #increase histInd
        
        return action
        
    def tuple_to_feildNum(tup):
        return tup[0] + 9*tup[1]
    
    def convert_board(pos):
        retArr = [n for n in pos.board]
        for n in pos.macroboard:
            if pos.board[n] > 0: # macro square is owned by player
                t = int((n%3)*3+round(n/3)*27)
                toMod = [t, t+1, t+2,  t+9, t+10, t+11,  t+18, t+19, t+20]
                for s in toMod:
                    retArr[s] = pos.board[n]
        return tuple(retArr)